# timesheet.api-master_solution

Web API Core service solution of GAC Timesheet assessment

Download the repo and perform a **update-database** to update the necessary database changes

Demo video of the application also included in the repo **(timesheet_demo.mp4)**