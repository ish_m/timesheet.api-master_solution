﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using timesheet.business.Interfaces;

namespace timesheet.api.Controllers
{
    [Route("api/v1/employee")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        #region Fields

        private readonly IEmployeeService employeeService;
        private readonly ILogger logger;

        #endregion

        #region Constructor

        public EmployeeController(IEmployeeService employeeService, ILogger<EmployeeController> logger)
        {
            this.employeeService = employeeService;
            this.logger = logger;
        }

        #endregion

        #region Service Endpoints

        [HttpGet("getallemployees")]
        public IActionResult GetAllEmployees()
        {
            try
            {
                var items = this.employeeService.GetAllEmployees();
                return new ObjectResult(items);
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "Error occurred while calling getallemployees");
                return StatusCode(500, string.Format("Internal server error - {0}", ex.Message));
            }
        }

        [HttpGet("getallemployeessummary")]
        public IActionResult GetAllEmployeeSummaryList()
        {
            try
            {
                var items = this.employeeService.GetAllEmployeesSummary();
                return new ObjectResult(items);
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "Error occurred while calling getallemployeessummary");
                return StatusCode(500, string.Format("Internal server error - {0}", ex.Message));
            }
        }

        #endregion
    }
}