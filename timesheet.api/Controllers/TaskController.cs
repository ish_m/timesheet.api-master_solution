﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using timesheet.business.Interfaces;

namespace timesheet.api.Controllers
{
    [Route("api/v1/task")]
    [ApiController]
    public class TaskController : ControllerBase
    {
        #region Fields

        private readonly ITaskService taskService;
        private readonly ILogger logger;

        #endregion

        #region Constructor

        public TaskController(ITaskService taskService, ILogger<TaskController> logger)
        {
            this.taskService = taskService;
            this.logger = logger;
        }

        #endregion

        #region Service Endpoints

        [HttpGet("getalltask")]
        public IActionResult GetAllTask(string text)
        {
            try
            {
                var items = this.taskService.GetAllTask();
                return new ObjectResult(items);
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "Error occurred while calling GetAllTask");
                return StatusCode(500, string.Format("Internal server error - {0}", ex.Message));
            }
        }

        #endregion
    }
}