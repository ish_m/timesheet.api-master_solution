﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using timesheet.business.Interfaces;
using timesheet.model.ViewModels;

namespace timesheet.api.Controllers
{
    [Route("api/v1/timesheet")]
    [ApiController]
    public class TimesheetController : ControllerBase
    {
        #region Fields

        private readonly ITimesheetService timesheetService;
        private readonly ILogger logger;

        #endregion

        #region Constructor

        public TimesheetController(ITimesheetService timesheetService, ILogger<TimesheetController> logger)
        {
            this.timesheetService = timesheetService;
            this.logger = logger;
        }

        #endregion

        #region Service Endpoints

        [HttpGet("getemployeetimesheets/{employeeId}/{weekStartDate}/{weekEndDate}")]
        public IActionResult GetEmployeeTimesheets(int employeeId, DateTime weekStartDate, DateTime weekEndDate)
        {
            try
            {
                var items = this.timesheetService.GetEmployeeTimesheets(employeeId, weekStartDate, weekEndDate);
                return new ObjectResult(items);
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "Error occurred while calling GetEmployeeTimesheets EmpId-{0}, WeekStartDate-{1}, WeekEndDate-{2}", employeeId, weekStartDate.ToShortDateString(), weekEndDate.ToShortDateString());
                return StatusCode(500, string.Format("Internal server error - {0}", ex.Message));
            }
        }

        [HttpPost("insertupdatetimesheets")]
        public IActionResult InsertUpdateTimesheets(List<TimesheetVM> timesheetVMList)
        {
            try
            {
                var items = this.timesheetService.InsertUpdateTimesheets(timesheetVMList);
                return new ObjectResult(items);
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "Error occurred while calling InsertUpdateTimesheets");
                return StatusCode(500, string.Format("Internal server error - {0}", ex.Message));
            }
        }

        #endregion
    }
}