﻿using System;
using System.Collections.Generic;
using System.Linq;
using timesheet.business.Interfaces;
using timesheet.data;
using timesheet.data.Interfaces;
using timesheet.data.Repositories;
using timesheet.model;
using timesheet.model.ViewModels;

namespace timesheet.business
{
    public class EmployeeService : IEmployeeService
    {
        #region Fields

        private IEmployeeRepository employeeRepository;
        private ITimesheetRepository timesheetRepository;

        #endregion

        #region Constructor

        public EmployeeService(IEmployeeRepository employeeRepository, ITimesheetRepository timesheetRepository)
        {
            this.employeeRepository = employeeRepository;
            this.timesheetRepository = timesheetRepository;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns employee effort summary data for employee list grid
        /// </summary>
        /// <returns></returns>
        public List<EmployeeSummaryVM> GetAllEmployeesSummary()
        {
            var employeeSummaryVMList = new List<EmployeeSummaryVM>();

            //Defining base date as today's date for calculation
            var baseDate = DateTime.Today;

            //Calculating the week start date and end date for calculation considering sunday as week start day
            var weekStartDate = baseDate.AddDays(-(int)baseDate.DayOfWeek).Date;
            var weekEndDate = weekStartDate.AddDays(7).AddSeconds(-1).Date;

            //Retrieving all employees and timesheets for processing
            var allEmployeeList = employeeRepository.GetAllEmployees().ToList();
            var allTimesheetList = timesheetRepository.GetAllTimesheets().ToList();

            //Processing each employee
            foreach (var employee in allEmployeeList)
            {
                var employeeSummaryVM = new EmployeeSummaryVM();
                employeeSummaryVM.Id = employee.Id;
                employeeSummaryVM.Code = employee.Code;
                employeeSummaryVM.Name = employee.Name;

                //Filtering the timesheets of the employee
                var employeeTimesheets = allTimesheetList.Where(t => t.EmployeeId == employee.Id).ToList();
                if (employeeTimesheets.Any())
                {
                    //Calculating employee weekly effort
                    employeeSummaryVM.TotalWeeklyEffort = employeeTimesheets.Where(t => t.Date >= weekStartDate && t.Date <= weekEndDate).Sum(c => c.Effort);

                    //Calculating employee average weekly effort
                    var minDate = employeeTimesheets.OrderBy(c => c.Date).Select(i => i.Date).First();
                    var maxDate = employeeTimesheets.OrderByDescending(c => c.Date).Select(i => i.Date).First();

                    var totalDays = (maxDate - minDate).Days;
                    if (totalDays > 0)
                    {
                        decimal numberOfWeeks = totalDays / 7m;
                        employeeSummaryVM.AverageWeeklyEffort = Math.Round(employeeTimesheets.Sum(i => i.Effort) / Math.Ceiling(numberOfWeeks), 2);
                    }
                }

                employeeSummaryVMList.Add(employeeSummaryVM);
            }

            return employeeSummaryVMList;
        }

        /// <summary>
        /// Returns all employees in the system
        /// </summary>
        /// <returns></returns>
        public List<Employee> GetAllEmployees()
        {
            return employeeRepository.GetAllEmployees().ToList();
        }

        #endregion
    }
}
