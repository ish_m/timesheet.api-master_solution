﻿using System;
using System.Collections.Generic;
using System.Text;
using timesheet.model;
using timesheet.model.ViewModels;

namespace timesheet.business.Interfaces
{
    public interface IEmployeeService
    {
        /// <summary>
        /// Returns employee effort summary data for employee list grid
        /// </summary>
        /// <returns></returns>
        List<EmployeeSummaryVM> GetAllEmployeesSummary();

        /// <summary>
        /// Returns all employees in the system
        /// </summary>
        /// <returns></returns>
        List<Employee> GetAllEmployees();
    }
}
