﻿using System;
using System.Collections.Generic;
using System.Text;
using timesheet.model;

namespace timesheet.business.Interfaces
{
    public interface ITaskService
    {
        /// <summary>
        /// Returns all tasks in the system
        /// </summary>
        /// <returns></returns>
        List<Task> GetAllTask();
    }
}
