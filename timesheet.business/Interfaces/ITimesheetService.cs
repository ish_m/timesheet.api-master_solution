﻿using System;
using System.Collections.Generic;
using System.Text;
using timesheet.model.ViewModels;

namespace timesheet.business.Interfaces
{
    public interface ITimesheetService
    {
        /// <summary>
        /// Returns processed employee timesheet records filter by employee id and week
        /// </summary>
        /// <param name="employeeId"></param>
        /// <param name="weekStartDate"></param>
        /// <param name="weekEndDate"></param>
        /// <returns></returns>
        List<TimesheetVM> GetEmployeeTimesheets(int employeeId, DateTime weekStartDate, DateTime weekEndDate);

        /// <summary>
        /// Insert or updating the employee timesheet records
        /// </summary>
        /// <param name="timesheetVMList"></param>
        /// <returns></returns>
        ResponseVM InsertUpdateTimesheets(List<TimesheetVM> timesheetVMList);
    }
}
