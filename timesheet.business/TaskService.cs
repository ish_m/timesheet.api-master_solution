﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using timesheet.business.Interfaces;
using timesheet.data.Interfaces;
using timesheet.model;

namespace timesheet.business
{
    public class TaskService : ITaskService
    {
        #region Fields

        private ITaskRepository taskRepository;

        #endregion

        #region Constructor

        public TaskService(ITaskRepository taskRepository)
        {
            this.taskRepository = taskRepository;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns all tasks in the system
        /// </summary>
        /// <returns></returns>
        public List<Task> GetAllTask()
        {
            return taskRepository.GetAllTasks().ToList();
        }

        #endregion
    }
}
