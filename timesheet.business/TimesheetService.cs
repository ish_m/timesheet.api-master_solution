﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using timesheet.business.Interfaces;
using timesheet.data.Interfaces;
using timesheet.model;
using timesheet.model.ViewModels;

namespace timesheet.business
{
    public class TimesheetService : ITimesheetService
    {
        #region Fields

        private ITimesheetRepository timesheetRepository;

        #endregion

        #region Constructor

        public TimesheetService(ITimesheetRepository timesheetRepository)
        {
            this.timesheetRepository = timesheetRepository;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns processed employee timesheet records filter by employee id and week
        /// </summary>
        /// <param name="employeeId"></param>
        /// <param name="weekStartDate"></param>
        /// <param name="weekEndDate"></param>
        /// <returns></returns>
        public List<TimesheetVM> GetEmployeeTimesheets(int employeeId, DateTime weekStartDate, DateTime weekEndDate)
        {
            var timesheetVMList = new List<TimesheetVM>();

            //Retrieving timesheets for the employee by week
            var employeeTimesheetList = timesheetRepository.GetEmployeeTimesheets(employeeId, weekStartDate, weekEndDate);

            //Processing timesheets and creating the view models for binding
            foreach (var employeeTimesheet in employeeTimesheetList)
            {
                var timesheetVM = timesheetVMList.FirstOrDefault(t => t.TaskId == employeeTimesheet.TaskId);
                if (timesheetVM == null)
                {
                    timesheetVM = new TimesheetVM() { EmployeeId = employeeTimesheet.EmployeeId, TaskId = employeeTimesheet.TaskId, TaskName = employeeTimesheet.Task.Name };
                    timesheetVMList.Add(timesheetVM);
                }

                if (employeeTimesheet.Date.DayOfWeek == DayOfWeek.Sunday)
                {
                    timesheetVM.WeekStartDate = employeeTimesheet.Date;
                    timesheetVM.SundayEffort = employeeTimesheet.Effort;
                }
                else if (employeeTimesheet.Date.DayOfWeek == DayOfWeek.Monday)
                {
                    timesheetVM.MondayEffort = employeeTimesheet.Effort;
                }
                else if (employeeTimesheet.Date.DayOfWeek == DayOfWeek.Tuesday)
                {
                    timesheetVM.TuesdayEffort = employeeTimesheet.Effort;
                }
                else if (employeeTimesheet.Date.DayOfWeek == DayOfWeek.Wednesday)
                {
                    timesheetVM.WednesdayEffort = employeeTimesheet.Effort;
                }
                else if (employeeTimesheet.Date.DayOfWeek == DayOfWeek.Thursday)
                {
                    timesheetVM.ThursdayEffort = employeeTimesheet.Effort;
                }
                else if (employeeTimesheet.Date.DayOfWeek == DayOfWeek.Friday)
                {
                    timesheetVM.FridayEffort = employeeTimesheet.Effort;
                }
                else if (employeeTimesheet.Date.DayOfWeek == DayOfWeek.Saturday)
                {
                    timesheetVM.SaturdayEffort = employeeTimesheet.Effort;
                }
            }

            return timesheetVMList;
        }

        /// <summary>
        /// Insert or updating the employee timesheet records
        /// </summary>
        /// <param name="timesheetVMList"></param>
        /// <returns></returns>
        public ResponseVM InsertUpdateTimesheets(List<TimesheetVM> timesheetVMList)
        {
            //Business logic validation for check whether duplicate task exists
            var taskGroup = timesheetVMList.GroupBy(t => t.TaskId)
            .Select(t => new
            {
                TaskId = t.Key,
                Count = t.Count()
            });

            if (taskGroup.Any(t => t.Count > 1))
            {
                return new ResponseVM() { IsSuccess = false, ValidationMessage = Resource.ResourceManager.GetString("DuplicateTaskValidationMessage") };
            }

            //Business logic validation for check whether daily effort exceeds 24H
            if (timesheetVMList.Sum(t => t.SaturdayEffort) >= 24)
            {
                return new ResponseVM() { IsSuccess = false, ValidationMessage = string.Format(Resource.ResourceManager.GetString("InvalidEffortValidationMessage"), DayOfWeek.Sunday) };
            }
            else if (timesheetVMList.Sum(t => t.MondayEffort) >= 24)
            {
                return new ResponseVM() { IsSuccess = false, ValidationMessage = string.Format(Resource.ResourceManager.GetString("InvalidEffortValidationMessage"), DayOfWeek.Monday) };
            }
            else if (timesheetVMList.Sum(t => t.TuesdayEffort) >= 24)
            {
                return new ResponseVM() { IsSuccess = false, ValidationMessage = string.Format(Resource.ResourceManager.GetString("InvalidEffortValidationMessage"), DayOfWeek.Tuesday) };
            }
            else if (timesheetVMList.Sum(t => t.WednesdayEffort) >= 24)
            {
                return new ResponseVM() { IsSuccess = false, ValidationMessage = string.Format(Resource.ResourceManager.GetString("InvalidEffortValidationMessage"), DayOfWeek.Wednesday) };
            }
            else if (timesheetVMList.Sum(t => t.ThursdayEffort) >= 24)
            {
                return new ResponseVM() { IsSuccess = false, ValidationMessage = string.Format(Resource.ResourceManager.GetString("InvalidEffortValidationMessage"), DayOfWeek.Thursday) };
            }
            else if (timesheetVMList.Sum(t => t.FridayEffort) >= 24)
            {
                return new ResponseVM() { IsSuccess = false, ValidationMessage = string.Format(Resource.ResourceManager.GetString("InvalidEffortValidationMessage"), DayOfWeek.Friday) };
            }
            else if (timesheetVMList.Sum(t => t.SaturdayEffort) >= 24)
            {
                return new ResponseVM() { IsSuccess = false, ValidationMessage = string.Format(Resource.ResourceManager.GetString("InvalidEffortValidationMessage"), DayOfWeek.Saturday) };
            }

            //Mapping the view models to domain objects and processing the records
            var timesheetList = new List<Timesheet>();
            foreach (var timesheetVM in timesheetVMList)
            {
                var timesheetSunday = new Timesheet() { EmployeeId = timesheetVM.EmployeeId, TaskId = timesheetVM.TaskId, Date = timesheetVM.WeekStartDate, Effort = timesheetVM.SundayEffort };
                timesheetList.Add(timesheetSunday);

                var timesheetMonday = new Timesheet() { EmployeeId = timesheetVM.EmployeeId, TaskId = timesheetVM.TaskId, Date = timesheetVM.WeekStartDate.AddDays(1), Effort = timesheetVM.MondayEffort };
                timesheetList.Add(timesheetMonday);

                var timesheetTuesday = new Timesheet() { EmployeeId = timesheetVM.EmployeeId, TaskId = timesheetVM.TaskId, Date = timesheetVM.WeekStartDate.AddDays(2), Effort = timesheetVM.TuesdayEffort };
                timesheetList.Add(timesheetTuesday);

                var timesheetWednesday = new Timesheet() { EmployeeId = timesheetVM.EmployeeId, TaskId = timesheetVM.TaskId, Date = timesheetVM.WeekStartDate.AddDays(3), Effort = timesheetVM.WednesdayEffort };
                timesheetList.Add(timesheetWednesday);

                var timesheetThursday = new Timesheet() { EmployeeId = timesheetVM.EmployeeId, TaskId = timesheetVM.TaskId, Date = timesheetVM.WeekStartDate.AddDays(4), Effort = timesheetVM.ThursdayEffort };
                timesheetList.Add(timesheetThursday);

                var timesheetFriday = new Timesheet() { EmployeeId = timesheetVM.EmployeeId, TaskId = timesheetVM.TaskId, Date = timesheetVM.WeekStartDate.AddDays(5), Effort = timesheetVM.FridayEffort };
                timesheetList.Add(timesheetFriday);

                var timesheetSaturday = new Timesheet() { EmployeeId = timesheetVM.EmployeeId, TaskId = timesheetVM.TaskId, Date = timesheetVM.WeekStartDate.AddDays(6), Effort = timesheetVM.SaturdayEffort };
                timesheetList.Add(timesheetSaturday);
            }

            timesheetRepository.InsertUpdateTimesheets(timesheetList);

            return new ResponseVM() { IsSuccess = true };
        }

        #endregion
    }
}
