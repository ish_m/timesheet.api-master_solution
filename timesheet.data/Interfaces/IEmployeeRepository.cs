﻿using System;
using System.Collections.Generic;
using System.Text;
using timesheet.model;

namespace timesheet.data.Interfaces
{
    public interface IEmployeeRepository
    {
        /// <summary>
        /// Returns all employees in the system
        /// </summary>
        /// <returns></returns>
        IEnumerable<Employee> GetAllEmployees();
    }
}
