﻿using System;
using System.Collections.Generic;
using System.Text;
using timesheet.model;

namespace timesheet.data.Interfaces
{
    public interface ITaskRepository
    {
        /// <summary>
        /// Returns all tasks in the system
        /// </summary>
        /// <returns></returns>
        IEnumerable<Task> GetAllTasks();
    }
}
