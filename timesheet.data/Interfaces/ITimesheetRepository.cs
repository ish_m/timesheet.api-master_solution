﻿using System;
using System.Collections.Generic;
using System.Text;
using timesheet.model;

namespace timesheet.data.Interfaces
{
    public interface ITimesheetRepository
    {
        /// <summary>
        /// Returns all timesheets in the system
        /// </summary>
        /// <returns></returns>
        IEnumerable<Timesheet> GetAllTimesheets();

        /// <summary>
        /// Returns filtered timesheet by employee and week
        /// </summary>
        /// <param name="employeeId"></param>
        /// <param name="weekStartDate"></param>
        /// <param name="weekEndDate"></param>
        /// <returns></returns>
        IEnumerable<Timesheet> GetEmployeeTimesheets(int employeeId, DateTime weekStartDate, DateTime weekEndDate);

        /// <summary>
        /// Insert or update timesheet 
        /// </summary>
        /// <param name="timesheetList"></param>
        /// <returns></returns>
        void InsertUpdateTimesheets(IEnumerable<Timesheet> timesheetList);
    }
}
