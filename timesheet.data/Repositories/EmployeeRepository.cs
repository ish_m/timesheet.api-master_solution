﻿using System;
using System.Collections.Generic;
using System.Text;
using timesheet.data.Interfaces;
using timesheet.model;

namespace timesheet.data.Repositories
{
    public class EmployeeRepository : IEmployeeRepository
    {
        #region Fields

        private readonly TimesheetDb timesheetDb;

        #endregion

        #region Constructor

        public EmployeeRepository(TimesheetDb timesheetDb)
        {
            this.timesheetDb = timesheetDb;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns all employees in the system
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Employee> GetAllEmployees()
        {
            return timesheetDb.Employees;
        }

        #endregion
    }
}
