﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using timesheet.data.Interfaces;
using timesheet.model;

namespace timesheet.data.Repositories
{
    public class TaskRepository : ITaskRepository
    {
        #region Fields

        private readonly TimesheetDb timesheetDb;

        #endregion

        #region Constructor

        public TaskRepository(TimesheetDb timesheetDb)
        {
            this.timesheetDb = timesheetDb;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns all tasks in the system
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Task> GetAllTasks()
        {
            return timesheetDb.Tasks;
        }

        #endregion
    }
}
