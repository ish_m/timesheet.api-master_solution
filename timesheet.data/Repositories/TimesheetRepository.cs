﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using timesheet.data.Interfaces;
using timesheet.model;

namespace timesheet.data.Repositories
{
    public class TimesheetRepository : ITimesheetRepository
    {
        #region Fields

        private readonly TimesheetDb timesheetDb;

        #endregion

        #region Constructor

        public TimesheetRepository(TimesheetDb timesheetDb)
        {
            this.timesheetDb = timesheetDb;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns all timesheets in the system
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Timesheet> GetAllTimesheets()
        {
            return timesheetDb.Timesheets;
        }

        /// <summary>
        /// Returns filtered timesheet by employee and week
        /// </summary>
        /// <param name="employeeId"></param>
        /// <param name="weekStartDate"></param>
        /// <param name="weekEndDate"></param>
        /// <returns></returns>
        public IEnumerable<Timesheet> GetEmployeeTimesheets(int employeeId, DateTime weekStartDate, DateTime weekEndDate)
        {
            return timesheetDb.Timesheets.Include(t => t.Task).Where(t => t.EmployeeId == employeeId && t.Date >= weekStartDate && t.Date <= weekEndDate);
        }

        /// <summary>
        /// Insert or update timesheet 
        /// </summary>
        /// <param name="timesheetList"></param>
        /// <returns></returns>
        public void InsertUpdateTimesheets(IEnumerable<Timesheet> timesheetList)
        {
            foreach (var timesheet in timesheetList)
            {
                var record = timesheetDb.Timesheets.Where(t => t.EmployeeId == timesheet.EmployeeId && t.Date == timesheet.Date && t.TaskId == timesheet.TaskId).FirstOrDefault();
                if (record != null)
                {
                    record.Effort = timesheet.Effort;
                    timesheetDb.Update(record);
                }
                else
                {
                    timesheetDb.Add(timesheet);
                }
            }

            timesheetDb.SaveChanges();
        }

        #endregion
    }
}
