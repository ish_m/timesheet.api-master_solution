﻿using Microsoft.EntityFrameworkCore;
using System;
using timesheet.model;

namespace timesheet.data
{
    public class TimesheetDb : DbContext
    {
        public TimesheetDb(DbContextOptions<TimesheetDb> options)
            : base(options)
        {
        }

        public DbSet<Employee> Employees { get; set; }
        public DbSet<Task> Tasks { get; set; }
        public DbSet<Timesheet> Timesheets { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Defining the type precision
            modelBuilder.Entity<Timesheet>().Property(r => r.Effort)
            .HasColumnType("decimal(6,4)");

            //Defining the FK relationships
            modelBuilder.Entity<Timesheet>()
            .HasOne(p => p.Task)
            .WithMany(b => b.Timesheet)
            .HasForeignKey(p => p.TaskId)
            .HasConstraintName("FK_Timesheet_Task");

            modelBuilder.Entity<Timesheet>()
            .HasOne(p => p.Employee)
            .WithMany(b => b.Timesheet)
            .HasForeignKey(p => p.EmployeeId)
            .HasConstraintName("FK_Timesheet_Employee");
        }
    }
}
