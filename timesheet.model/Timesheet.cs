﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace timesheet.model
{
    public class Timesheet
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public long Id { get; set; }

        [Required]
        public int EmployeeId { get; set; }

        [Required]
        public int TaskId { get; set; }

        [Required]
        public DateTime Date { get; set; }

        [Required]
        public decimal Effort { get; set; }

        public Task Task { get; set; }

        public Employee Employee { get; set; }
    }
}
