﻿using System;
using System.Collections.Generic;
using System.Text;

namespace timesheet.model.ViewModels
{
    /// <summary>
    /// Contains data for employee list grid
    /// </summary>
    public class EmployeeSummaryVM
    {
        public int Id { get; set; }

        public string Code { get; set; }

        public string Name { get; set; }

        public decimal TotalWeeklyEffort { get; set; }

        public decimal AverageWeeklyEffort { get; set; }
    }
}
