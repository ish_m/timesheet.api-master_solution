﻿using System;
using System.Collections.Generic;
using System.Text;

namespace timesheet.model.ViewModels
{
    /// <summary>
    /// Contains data for business logic validation
    /// </summary>
    public class ResponseVM
    {
        public bool IsSuccess { get; set; }

        public string ValidationMessage { get; set; }
    }
}
