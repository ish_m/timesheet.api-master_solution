﻿using System;
using System.Collections.Generic;
using System.Text;

namespace timesheet.model.ViewModels
{
    /// <summary>
    /// Contains data for timesheet entry screen
    /// </summary>
    public class TimesheetVM
    {
        public long Id { get; set; }

        public int TaskId { get; set; }

        public int EmployeeId { get; set; }

        public string TaskName { get; set; }

        public decimal SundayEffort { get; set; }

        public decimal MondayEffort { get; set; }

        public decimal TuesdayEffort { get; set; }

        public decimal WednesdayEffort { get; set; }

        public decimal ThursdayEffort { get; set; }

        public decimal FridayEffort { get; set; }

        public decimal SaturdayEffort { get; set; }

        public DateTime WeekStartDate { get; set; }
    }
}
